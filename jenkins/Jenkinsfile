pipeline {
    agent none
    options {
        timeout(time: 2, unit: 'HOURS')
    }
    environment {
        JK_GIT_CRED = "git"
        JK_SVN_CRED = "svn"
    }
    stages {
        stage('Build The Code') {
            parallel {
                stage('Linux') {
                    agent {
                        label "linux"
                    }
                    stages {
                        stage('Get code from SCM - linux') {
                            steps {
                                dir('projRoot') {
                                    deleteDir()
				}
                                sh 'mkdir -p projRoot/sources/jk_git'
                                dir('projRoot/sources/jk_git') {
                                    git credentialsId: "${JK_GIT_CRED}", branch: 'develop', url: 'https://some.url.com/general/jenkins.git'
                                }
                                dir('projRoot/sources') {
                                    checkout([$class: 'SubversionSCM',
                                        additionalCredentials: [],
                                        excludedCommitMessages: '',
                                        excludedRegions: '',
                                        excludedRevprop: '',
                                        excludedUsers: '',
                                        filterChangelog: false,
                                        ignoreDirPropChanges: false,
                                        includedRegions: '',
                                        locations: [[credentialsId: "${JK_SVN_CRED}",
                                                depthOption: 'infinity',
                                                ignoreExternalsOption: true,
                                                local: 'jk_svn',
                                                remote: 'https://some.other.url/svn/repo_for_jenkins']],
                                                quietOperation: false,
                                    workspaceUpdater: [$class: 'CheckoutUpdater']])
                                }
                            }
                        }
                        stage('Build the code - linux') {
                            steps {
                                dir('projRoot') {
                                    sh 'python3 sources/jk_git/scripts/buildCI.py'
                                }
                            }
                        }
                        stage('Valgrind - linux') {
                            steps {
				                runValgrind (
				                   childSilentAfterFork: true,
				                    excludePattern: '',
				                    generateSuppressions: true,
				                    ignoreExitCode: true,
				                    includePattern: 'bin/test_leak',
				                    outputDirectory: '',
				                    outputFileEnding: '.memcheck',
				                    programOptions: '',
				                    removeOldReports: true,
				                    suppressionFiles: '',
				                    tool: [$class: 'ValgrindToolMemcheck',
				                    leakCheckLevel: 'full',
				                    showReachable: true,
				                    trackOrigins: true,
				                    undefinedValueErrors: true],
				                    traceChildren: true,
				                    valgrindExecutable: '',
				                    valgrindOptions: '',
				                    workingDirectory: ''
				                )

				                publishValgrind (
				                    failBuildOnInvalidReports: false,
				                    failBuildOnMissingReports: false,
				                    failThresholdDefinitelyLost: '',
				                    failThresholdInvalidReadWrite: '',
				                    failThresholdTotal: '',
				                    pattern: '*.memcheck',
				                    publishResultsForAbortedBuilds: false,
				                    publishResultsForFailedBuilds: false,
				                    sourceSubstitutionPaths: '',
				                    unstableThresholdDefinitelyLost: '',
				                    unstableThresholdInvalidReadWrite: '',
				                    unstableThresholdTotal: ''
				                )
                            }
			            }
                    }
                    post('Post build actions - linux') {
                        success {
                            echo 'linux build is successful!'
                        }
                        unstable {
                            echo 'linux build is unstable!'
                        }
                        failure {
                            echo 'linux build failed!'
                        }
                        always {
                            echo 'sending an email for linux'
                            //emailext attachLog: true, body: 'linux test body', postsendScript: 'echo "Bye post-send script!"', presendScript: 'echo "Hello pre-send script!"', subject: 'test-linux', to: 'devops@greenthing.dev'
                            }
                    }
                }
                stage('Windows') {
                    agent {
                        label "windows"
                    }
                    stages {
                        stage('Get code from SCM - windows') {
                            steps {
                                dir('projRoot') {
                                    deleteDir()
				}
                                bat 'mkdir projRoot\\sources\\jk_git'
                                dir('projRoot\\sources\\jk_git') {
                                    git credentialsId: "${JK_GIT_CRED}", branch: 'develop', url: 'https://some.url.com/general/jenkins.git'
                                }
                                dir('projRoot\\sources') {
                                    checkout([$class: 'SubversionSCM',
                                        additionalCredentials: [],
                                        excludedCommitMessages: '',
                                        excludedRegions: '',
                                        excludedRevprop: '',
                                        excludedUsers: '',
                                        filterChangelog: false,
                                        ignoreDirPropChanges: false,
                                        includedRegions: '',
                                        locations: [[credentialsId: "${JK_GIT_CRED}",
                                                    depthOption: 'infinity',
                                                    ignoreExternalsOption: true,
                                                    local: 'jk_svn',
                                                    remote: 'https://some.other.url.com/svn/repo_for_jenkins']],
                                                    quietOperation: false,
                                    workspaceUpdater: [$class: 'CheckoutUpdater']])
                                }
                            }
                        }
                        stage('Build the code - windows') {
                            steps {
                                dir('projRoot') {
                                    bat 'c:\\python\\python.exe sources\\jk_git\\scripts\\buildCI.py'
                                }
                            }
                        }
                    }
                    post('Post-build actions - windows') {
                        success {
                            echo 'windows build is successful!'
                            //dir('projRoot') {
                            //    bat 'c:\\python\\python.exe sources\\jk_git\\scripts\\myscript.py'
                            //}
                        }
                        unstable {
                            echo 'windows build is unstable!'
                        }
                        failure {
                            echo 'windows build failed!'
                        }
                        always {
                            echo 'sending an email for windows'
                            //emailext attachLog: true, body: 'win test body', postsendScript: 'echo "Bye post-send script!"', presendScript: 'echo "Hello pre-send script!"', subject: 'test-win', to: 'devops@greenthing.dev'
                        }
                    }
                }
            }
        }
    }
}